import Vue from 'vue'
import Vuex from 'vuex'
import { db, auth } from "../firebase";
import router from '../router/index'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tareas: [],
    tarea: {
      nombre: '',
      id: '',
    },
    usuario: null,
    error: null,
    carga: false,
    texto: ''
  },
  mutations: {
    setTareas(state, tareas) {
      state.tareas = tareas
    },
    setTarea(state, tarea) {
      state.tarea = tarea
    },
    setUsuario(state, usuario) {
      state.usuario = usuario
    },
    setError(state, error) {
      state.error = error
    },
    cargarFirebase(state, estado) {
      state.carga = estado
    }
  },
  actions: {
    getTareas({commit, state}) {
      commit('cargarFirebase', true)
      const tareas = []
      db.collection(state.usuario.uid).get()
        .then(res => {
          res.forEach(item => {
            // console.log(item.id, item.data());
            let tarea = item.data()
            tarea.id = item.id
            tareas.push(tarea)
          })
          commit('cargarFirebase', false)
        })
        commit('setTareas', tareas)
    },
    getTarea({commit, state}, id) {
      commit('cargarFirebase', true)
      db.collection(state.usuario.uid).doc(id).get()
        .then(item => {
          let tarea = item.data()
          tarea.id = item.id
          commit('setTarea', tarea)
          commit('cargarFirebase', false)
        })
    },
    editarTarea({commit, state}, tarea) {
      db.collection(state.usuario.uid).doc(tarea.id).update({
        nombre: tarea.nombre
      })
      .then(() => {
        router.push('/')
      })
    },
    agregarTarea({commit, state}, nombre) {
      commit('cargarFirebase', true)
      db.collection(state.usuario.uid).add({
        nombre: nombre
      })
      .then(item => {
        commit('cargarFirebase', false)
        router.push('/')
      })
    },
    eliminarTarea({commit, dispatch, state}, id) {
      db.collection(state.usuario.uid).doc(id).delete()
      .then(() => {
        dispatch('getTareas')
      })
    },
    registrarUsuario({commit}, usuario) {
      auth.createUserWithEmailAndPassword(usuario.email, usuario.pass)
        .then(res => {
          const user = {
            email: res.user.email,
            uid: res.user.uid
          }

          db.collection(user.uid).add({
            nombre: 'tarea de ejemplo'
          })

          commit('setUsuario', user)
          router.push('/')
        })
        .catch(e => {
          commit('setError', e)
        })
    },
    ingresoUsuario({commit}, usuario) {
      auth.signInWithEmailAndPassword(usuario.email, usuario.pass)
        .then(res => {
          const user = {
            email: res.user.email,
            uid: res.user.uid
          }
          commit('setUsuario', user)
          router.push('/')
        })
        .catch(e => {
          commit('setError', e)
        })
    },
    cerrarSesion({commit}) {
      auth.signOut()
        .then(() => {
          router.push('/acceso')
        })
    },
    detectarUsuario({commit}, usuario) {
      commit('setUsuario', usuario)
    },
    buscador({commit, state}, cadena) {
      state.texto = cadena.toLowerCase()
    }
  },
  getters: {
    existeUsuario(state) {
      if (state.usuario === null) {
        return false
      }
      else {
        return true
      }
    },
    arrayFiltrado(state) {
      let arregloFiltrado = []
      for (let tarea of state.tareas) {
        let nombre = tarea.nombre.toLowerCase()
        if (nombre.indexOf(state.texto) >= 0) {
          arregloFiltrado.push(tarea)
        }
      }
      return arregloFiltrado
    }
  },
  modules: {
  }
})
