import firebase from "firebase/app";
import firestore from "firebase/firestore";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyD6pqXpfEyQ5EVRZD-Bs-z-DRDHkKNL9n8",
  authDomain: "vue2-ea788.firebaseapp.com",
  projectId: "vue2-ea788",
  storageBucket: "vue2-ea788.appspot.com",
  messagingSenderId: "745558861095",
  appId: "1:745558861095:web:5ef84c8281c6e6d2f57502"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();

export { db, auth }
