# Tareas

_Proyecto realizado en curso de [udemy](https://www.udemy.com/course/curso-vue/) con bluuweb._

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
